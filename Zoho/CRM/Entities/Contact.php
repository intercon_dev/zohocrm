<?php
/**
 * Created by PhpStorm.
 * User: kento
 * Date: 3/11/15
 * Time: 10:27 AM
 */

namespace Zoho\CRM\Entities;


use Zoho\CRM\Wrapper\Element;

class Contact extends Element {

    /**
     * Zoho CRM user to whom the Lead is assigned.
     *
     * @var string
     */
    
    
    public $Contact_Owner;

    public $ContactID;

    public $OnDeck_Submission_Status;
    public $OnDeck_ApplicationNumber;
    public $Best_Offer;

    public $TrustPilot2;
    public $TrustPilot;
    /**
     * Salutation for the lead
     *
     * @var string
     */
    //public $Salutation;

//    //public $MCC_App_ID;
//
//    public $MCC_Submission_Status;
//
//    public $Selected_Option;
//
//    public $MCC_Options;
//
//    /**
//     * First name of the lead
//     *
//     * @var string
//     */
//    public $First_Name;
//
//    /**
//     * The job position of the lead
//     *
//     * @var string
//     */
//    public $Title;
//
//    /**
//     * Last name of the lead
//     *
//     * @var string
//     */
//    public $Last_Name;
//
//    /**
//     * Name of the company where the lead is working.
//     * This field is a mandatory
//     *
//     * @var string
//     */
//    public $Company;
//
//    /**
//     * Source of the lead, that is, from where the lead is generated
//     *
//     * @var string
//     */
//    public $Lead_Source;
//
//    /**
//     * Industry to which the lead belongs
//     *
//     * @var string
//     */
//    public $Industry;
//
//    /**
//     * Annual revenue of the company where the lead is working
//     *
//     * @var integer
//     */
//    public $Annual_Revenue;
//
//    /**
//     * Phone number of the lead
//     *
//     * @var string
//     */
//    public $Phone;
//
//    /**
//     * Modile number of the lead
//     *
//     * @var string
//     */
//    public $Mobile;
//
//    /**
//     * Fax number of the lead
//     *
//     * @var string
//     */
//    public $Fax;
//
//    /**
//     * Email address of the lead
//     *
//     * @var string
//     */
//    public $Email;
//
//    /**
//     * Secundary email address of the lead
//     *
//     * @var string
//     */
//    public $Secundary_Email;
//
//    /**
//     * Skype ID of the lead. Currently skype ID
//     * can be in the range of 6 to 32 characters
//     *
//     * @var string
//     */
//    public $Skype_ID;
//
//    /**
//     * Web site of the lead
//     *
//     * @var string
//     */
//    public $Website;
//
//    /**
//     * Status of the lead
//     *
//     * @var string
//     */
//    public $Lead_Status;
//
//    /**
//     * Rating of the lead
//     *
//     * @var string
//     */
//    public $Rating;
//
//    /**
//     * Number of employees in lead's company
//     *
//     * @var integer
//     */
//    public $No_of_Employees;
//
//    /**
//     * Remove leads from your mailing list so that they will
//     * not receive any emails from your Zoho CRM account
//     *
//     * @var string
//     */
//    public $Email_Opt_Out;
//
//    /**
//     * Campaign related to the Lead
//     *
//     * @var string
//     */
//    public $Campaing_Source;
//
//    /**
//     * Street address of the lead
//     *
//     * @var string
//     */
//    public $Street;
//
//    /**
//     * Name of the city where the lead lives
//     *
//     * @var string
//     */
//    public $City;
//
//    /**
//     * Name of the state where the lead lives
//     *
//     * @var string
//     */
//    public $State;
//
//    /**
//     * Postal code of the lead's address
//     *
//     * @var string
//     */
//    public $Zip_Code;
//
//    /**
//     * Name of the lead's country
//     *
//     * @var string
//     */
//    public $Country;
//
//    /**
//     * Other details about the lead
//     *
//     * @var string
//     */
//    public $Description;
//
//    /**
//     * Getter
//     *
//     * @return mixed
//     */
//
//    /**
//     * Business Info
//     */
//    public $DBA,$Business_Start_Date;
//    public $Business_Type;
//    public $Length_of_Ownership;
//
//    /**
//     * Funding Information
//     */
//    public $Amount_Requested;
//    public $CC_Sales_Monthly;
//    public $Monthly_Gross_Sales;
//    public $Average_Daily_Bank;
//    public $Lead_Date;
//    /**
//     * Business Reference
//     */
//    public $Trade_Reference_1, $Trade_Reference_2, $Trade_Reference_3, $Bank_Reference;
//    public $Tax_ID;
//
//    /**
//     *Owner information
//     *
//     */
//    public $Owner_Street,$Owner_City,$Owner_State,$Owner_Zip;
//    public $Owner_Email,$Owner_Phone,$Owner_Mobile;
//    public $Percentage_of_Ownership;
//    public $SSN;
//
//    public $Signature_Url,$Suite,$Credit_Score_FICO,$Landlord_Mortgage_Company;
//    public $Monthly_Rent_Mortgage_Payment,$Landlord_Mortgage_Contact_Name,$Landlord_Mortgage_Contact_Phone;
//    public $Ever_used_cash_advance_before,$Cash_Advance_Provider,$Current_Cash_Advance,$Average_Ticket_Size;
//    public $Amex_MID_Number,$Discover_MID_Number,$Terminal_POS_System,$Products_Services_Sold;
//    public $Date_of_Birth;



    public function __get($property)
    {
        return isset($this->$property)?$this->$property :null;
    }

    /**
     * Setter
     *
     * @param string $property Name of the property to set the value
     * @param mixed $value Value for the property
     * @return mixed
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
        return $this->$property;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: kento
 * Date: 22/05/15
 * Time: 04:40 PM
 */

namespace Zoho\CRM\Entities;

use Zoho\CRM\Wrapper\Element;

/**
 * Entity for accounts inside Zoho
 * This class only have default parameters
 *
 * @package Zoho\CRM\Entities
 * @version 1.0.0
 */
class Task extends Element
{

    public $Subject;

    public $Start_DateTime;

    public $End_DateTime;

    public $From;

    public $To;

    public $Type;

    public $Venue;

    public $Location;

    public $Send_Notification_Email;

    public $LEADID;

    public $CONTACTID;

    public $SEID;

    public $SEMODULE;

    public function __get($property)
    {
        return isset($this->$property)?$this->$property :null;
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
        return $this->$property;
    }
}
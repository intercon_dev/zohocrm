<?php
/**
 * Created by PhpStorm.
 * User: kento
 * Date: 30/12/15
 * Time: 10:51 AM
 */
namespace Zoho\CRM\Entities;

use Zoho\CRM\Wrapper\Element;

/**
 * Entity for Potential inside Zoho
 * This class only have default parameters
 *
 * @package Zoho\CRM\Entities
 * @version 1.0.0
 */
class CustomModule1 extends Element
{
    public $SMOWNERID;
    public $Contact_ID;
    public $Phone_NO;
    public $Message_Id;
    public $CustomModule1_Name;
    public $Message_time;
    public $Status;
    public $SMS_Response;

    public function __get($property)
    {
        return isset($this->$property)?$this->$property :null;
    }

    /**
     * Setter
     *
     * @param string $property Name of the property to set the value
     * @param mixed $value Value for the property
     * @return mixed
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
        return $this->$property;
    }

}